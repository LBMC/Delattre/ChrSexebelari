\documentclass[a4paper,12pt]{article}

\begin{document}

All the scripts and codes used for the bioinformatic analysis are freely available at https://github.com/LBMC/ChrSexebelari.

\section{Reads preparation}

In this section we describe the preprocessing of sequencing reads before their analyses.
We used a set of 200x coverage DNASeq short reads, a set of RNASeq short reads and a set of 14x DNASeq long reads.
We also used two set of DNASeq short read one for a population of female individuals and one for a population of male individuals (20x and 14x).

\subsection{Illumina Reads}

Two paired-end genomic libraries (insert sizes of 350 bp and 550 bp) were prepared for JU2817 using TruSeqNano with gelfree size selection (Nano 350 Gelfree and Nano 550 Gelfree).
The 350 bp library was sequenced on the HiSeqX and the 550 bp library was sequenced on the HiSeq4000 with $2 \times 100$ bp paired-end read length.
The sex-specific DNASeq library was sequenced on the HiSeq4000 (TruSeq® RNA Sample Preparation v2).
An additional RNA library was sequenced on Illumina Hiseq 4000 sequencer as paired-end 100 bp reads following Illumina instructions.
We removed Illumina adapter using cutadapt (v1.14, \texttt{-a AGATCGGAAGAG -g CTCTTCCGATCT -A AGATCGGAAGAG -G CTCTTCCGATCT}) \cite{martinCutadaptRemovesAdapter2011}.
The reads were subsequently trimmed by quality using UrQt (vd62c1f8, \texttt{--t 20}) \cite{modoloUrQtEfficientSoftware2015}.
We analysed the reads quality before and after adaptor removal and quality trimming using FastQC (v0.11.5) and MultiQC (v1.0) \cite{BabrahamBioinformaticsFastQC, ewelsMultiQCSummarizeAnalysis2016}.
We expected an {\it E.coli} OP50 contamination (used to feed the worms), which was confirmed by a bimonal GC\% distribution in mutilqc reports.
To remove {\it E.coli} related reads, we mapped the Illumina library on the \texttt{PRJNA41499} pubmed genome project using bowtie2 (v2.2.4, \texttt{--very-sensitive}) \cite{langmeadFastGappedreadAlignment2012a}.
We extracted unmapped reads using samtools (v1.3.1, \texttt{view -F 4 -b}) \cite{liSequenceAlignmentMap2009}.

\subsection{Minion Reads}

For the long reads, we used a set of 14x coverage reads obtained by the MinION sequencer, from Oxford Nanopore technologies.
We removed the {\it E.coli} OP50 reads using bwa mem (v0.7.17, \texttt{-x ont2d}).
We then extracted the unmapped reads samtools (v1.3.1, \texttt{view -F 4 -b}).
We evaluated the quality of the reads using nanostat \cite{decosterNanoPackVisualizingProcessing2018c}.

\section{De novo genome assembly}

We used DBG2OLC (v0246e46) to construct a hybrid assembly of the {\it M.belari} genome using the two Illumina library and the Minion library \cite{yeDBG2OLCEfficientAssembly2016b}.
We set the parameters for Sparse Assembler step to \texttt{g 15 k 51 LD 0 GS 200000000 NodeCovTh 2 EdgeCovTh 1} and
\texttt{k 17 MinLen 250 AdaptiveTh 0.001 KmerCovTh 3 MinOverlap 15 RemoveChimera 1} for the step 2 of consensus iterations.
We evaluated this assembly using Quast (v5.0.2)\cite{gurevichQUASTQualityAssessment2013a}, ALE (v9b0fadd) \cite{clarkALEGenericAssembly2013a} and by mapping the 550 bp library using bowtie2 (\texttt{--very-sensitive-local}).

\section{Genome annotation}

We first build an annotation of the repetitive regions of the genome using dnaPipeTE with the 550 bp Illumina library (vd3ee573, \texttt{-genome\_size 129550000 -genome\_coverage 0.1 -keep\_Trinity\_output}) \cite{goubertNovoAssemblyAnnotation2015a}.
We then used the output of dnaPipeTE to mask the assembled genome using and ReapeatMasker (v4.0.7, \texttt{-xsmall -lib dnapipeTE\_out.fasta}) \cite{smitRepeatMaskerOpen42013}.
We then mapped the 550 bp Illumina library on the hard masked genome using STAR (v2.5.2b, default options) \cite{dobinSTARUltrafastUniversal2013a}.
Finally, we used STAR mapping results and the soft masked genome to run BRAKER (v2.1.0, \texttt{--softmasking 1}) to create the gene annotation of the assembled genome \cite{hoffBRAKER1UnsupervisedRNASeqBased2016a}.
We converted the gff3 results of BRAKER to the bed format using gff2bed (v2.4.19) \cite{nephBEDOPSHighperformanceGenomic2012}.

\section{Coverage analysis}

To detect sex specific contigs, we mapped the sex-specific DNASeq library on the assembled genome using bowtie2 (\texttt{--very-sensitive}).
We filtered the alignments with samtools (\texttt{view -b -q 5}).
We computed the genome coverage for each sex with bedtools using the filtered alignments (v2.24.0, \texttt{bamtobed -i}) before computing the gene specific coverage using the bed annotation of the genome (\texttt{map -o count}).
Using R, \cite{rcoreteamLanguageEnvironmentStatistical2013} we normalized the male and female distribution using the normalize.quantiles from the preprocessCore package \cite{bolstadPreprocessCoreCollectionPreprocessing2018}.
We used custom functions to compute the mean squared error between the normalized male counts $x_i$ and the normalized female counts $y_i$ of gene $i$ and the 3 following models:

\begin{itemize}
  \item $y_i = x_i$ to model the expected sex bias of autosome
  \item $y_i = 0 \times x_i$ to model the expected sex bias of the Y chromosome
  \item $y_i = 2 \times x_i$ to model the expected sex bias of the X chromosome
\end{itemize}

We assigned the label of the model minimizing the mean squared error to the gene $i$, to determine which gene was autosomal, link to the Y chromosome or link to the X chromosome.

\section{SNP analysis}

To follow Y, X and autosomal chromosomes during reproduction we searched SNP present in the JU2859 strain and absent from the JU2817 strain.
We first filtered the assembled genome with bioawk (vfd40150) to work with contigs larger than 800 kb \cite{hengBioAWK2017}.
We mapped the sex mixed DNASeq library on the resulting contigs with bowtie2 (\texttt{--very-sensitive}, and the option \texttt{--rg-id --rg PL: --rg SM} to define the strain of the mapped reads).
We flagged the PCR duplicate with samblaster (v0.1.24, \texttt{--addMateTags -M}) \cite{faustSAMBLASTERFastDuplicate2014} and performed the bam conversion, sorting and indexing with sambamba (v0.6.7, \texttt{view --valid -S -f bam -l 0}) \cite{tarasovSambambaFastProcessing2015}.
We generated vcf file from the resulting bam with bcftools (v1.7, \texttt{mpileup -AE --output-type v -a FORMAT/AD,FORMAT/ADF,FORMAT/ADR,FORMAT/DP,FORMAT/SP,INFO/AD,INFO/ADF,INFO/ADR}) called without filters (\texttt{call -mv --output-type v}).
Finally, we converted the vcf to csv tables with gatk (v4.0.8.1, \texttt{VariantsToTable -F CHROM -F POS -F TYPE -GF GT -GF AD -F AD -F DP}).
We crossed the resulting SNP table using custom functions to extract SNP present in JU2859 but absent from JU2817.

\bibliographystyle{abbrv}
\bibliography{bibliography}

\end{document}
